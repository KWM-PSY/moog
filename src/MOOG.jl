# ===========================================================
# MODULE Moog
# ---------------
# In this module all server-client messages are registered as constants
#
# included struct definitions:
# - Port
#
# included functions:
# - sendmsg
# - loop
# - close
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
module MOOG

using Printf,Libdl

const SERVER_INPORT=1966
const SERVER_OUTPORT=1860

const MSG_LOGIN=0
const MSG_LOGIN_ACK=1
const MSG_LOGIN_NAK=2
const MSG_ENGAGE=3  
const MSG_ENGAGE_ACK=4
const MSG_ENGAGE_NAK=5
const MSG_ENGAGE_DONE=6
const MSG_PARK=7
const MSG_PARK_ACK=8
const MSG_PARK_NAK=9
const MSG_PARK_DONE=10
const MSG_LOGOUT=11
const MSG_LOGOUT_ACK=12
const MSG_LOGOUT_NAK=13
const MSG_IDLE_TIMEOUT=14
const MSG_LIST_SESSIONS=15
const MSG_LIST_SESSIONS_SESSION=16
const MSG_LIST_SESSIONS_NAK=17
const MSG_LIST_SESSIONS_DONE=18
const MSG_POST_PRODUCE=19
const MSG_POST_PRODUCE_NAK=20
const MSG_POST_PRODUCE_DONE=21
const MSG_ACTIVATE_HW_INPUT=22
const MSG_HW_INPUT_ACK=23
const MSG_HW_INPUT_NAK=24
const MSG_HW_INPUT_EVENT=25
const MSG_SEED_RNDGEN=26
const MSG_GET_BRANCH=27
const MSG_BRANCH=28
const MSG_GET_TOPIC=29
const MSG_TOPIC=30

const MSG_STATE=100
const MSG_DPIN_CHANGE=101

const MSG_IMMEDIATE_REQ=200
const MSG_IMMEDIATE_ACK=201
const MSG_IMMEDIATE_NAK=202
const MSG_IMMEDIATE_AT_HEIGHT=203

const MSG_BEGIN_SHORT_SEQ=300
const MSG_SHORT_SEQ_STEP=301
const MSG_COMPLETE_SHORT_SEQ=302
const MSG_SHORT_SEQ_ACK=303
const MSG_SHORT_SEQ_NAK=304
const MSG_SHORT_SEQ_AT_HEIGHT=305
const MSG_SHORT_SEQ_DONE=306
const MSG_SHORT_SEQ_FREEZE=307
const MSG_SHORT_SEQ_FREEZE_ACK=308

const MSG_BEGIN_LONG_SEQ=400
const MSG_LONG_SEQ_STEP=401
const MSG_COMPLETE_LONG_SEQ=402
const MSG_LONG_SEQ_ACK=403
const MSG_LONG_SEQ_NAK=404
const MSG_LONG_SEQ_AT_HEIGHT=405
const MSG_LONG_SEQ_AT_START=406
const MSG_LONG_SEQ_READY=407
const MSG_LONG_SEQ_EXECUTE=408
const MSG_LONG_SEQ_EXECUTE_NAK=409
const MSG_LONG_SEQ_DONE=410
const MSG_LONG_SEQ_FREEZE=411
const MSG_LONG_SEQ_FREEZE_ACK=412
const MSG_LONG_SEQ_STEP_SINEACC=413
const MSG_LONG_SEQ_STEP_DAMPEDOSC=414

const MSG_AV_AUDIO_ACK=600
const MSG_AV_AUDIO_NAK=601
const MSG_AV_OPEN_AUDIO=602
const MSG_AV_AUDIO_PLAY=603
const MSG_AV_AUDIO_STOP=604
const MSG_AV_AUDIO_STOP_ALL=605
const MSG_AV_AUDIO_SAMPLE_ENDED=606

const MSG_AV_STILLS_ACK=620
const MSG_AV_STILLS_NAK=621
const MSG_AV_OPEN_STILLS=622
const MSG_AV_STILLS_STILL=623
const MSG_AV_STILLS_FILL_RGB=624
const MSG_AV_STILLS_ACTIVATE=625
const MSG_AV_STILLS_DEACTIVATE=626
const MSG_AV_STILLS_GET_IPD=627
const MSG_AV_STILLS_SET_IPD=628
const MSG_AV_STILLS_IPD_VAL=629
const MSG_AV_STILLS_ROTATE_SCREEN=630

const MSG_AV_TRANSD_ACK=640
const MSG_AV_TRANSD_NAK=641
const MSG_AV_OPEN_TRANSD=642
const MSG_AV_TRANSD_PLAY=643
const MSG_AV_TRANSD_STOP=644
const MSG_AV_TRANSD_CHANNEL_IDLE=645

const MSG_AV_MOVIES_ACK=660
const MSG_AV_MOVIES_NAK=661
const MSG_AV_OPEN_MOVIES=662
const MSG_AV_MOVIES_MOVIE=663
const MSG_AV_MOVIES_ROTATE_SCREEN=664
const MSG_AV_MOVIES_PAUSE=665
const MSG_AV_MOVIES_FILL_RGB=666

const MSG_AV_INQUIRE=680
const MSG_AV_INQUIRE_REPLY=681

const SESSION_IMMEDIATE=1
const SESSION_SHORT_SEQ=2
const SESSION_LONG_SEQ=3
const SESSION_JOYSTICK=4

const SSTATE_OFF=-1
const SSTATE_IDLE=0
const SSTATE_ENGAGING=1
const SSTATE_ENGAGED=2
const SSTATE_PARKING=3
const SSTATE_FAULT=4
const SSTATE_FROZEN=5

const MACH_STATE_POWERUP=0
const MACH_STATE_IDLE=1
const MACH_STATE_STBY=2
const MACH_STATE_ENGAGED=3
const MACH_STATE_PARKING=7
const MACH_STATE_FAULT1=8
const MACH_STATE_FAULT2=9
const MACH_STATE_FAULT3=10
const MACH_STATE_DISABLED=11
const MACH_STATE_INHIBITED=12

const LAW_LINEAR='L'
const LAW_SINE='S'
const LAW_CASTELJAU='C'

mutable struct Port
  remport::Int
  sts1::Int
  sts2::Int
  sts3::Int
  wait_for_idle::Bool
  dls_sendpkt
  dls_recvpkt
  dls_close
  
  function Port()
    dlo=dlopen(Main.MOOGSKT_C_MODULE)
    dls_init=dlsym(dlo,:moogskt_init)
    dls_recvport=dlsym(dlo,:moogskt_recvport)
    dls_sendpkt=dlsym(dlo,:moogskt_send_pkt)
    dls_recvpkt=dlsym(dlo,:moogskt_recv_pkt)
    dls_close=dlsym(dlo,:moogskt_close)
    
    err=ccall(dls_init,Cint,(Cstring,Cstring,Cstring,Cstring,Cstring,Cint,Cint),Main.LOCAL_ADDRESS,
              Main.SERVER_MOOG_ADDRESS1,Main.SERVER_MOOG_ADDRESS2,Main.SERVER_MOOG_ADDRESS3,Main.SERVER_MOOG_ADDRESS4,
              SERVER_OUTPORT,SERVER_INPORT)
    if(err!=0)
      error("Could not contact platform")
    end
    port=ccall(dls_recvport,Cint,())
    
    new(port,-1,-1,-1,false,dls_sendpkt,dls_recvpkt,dls_close)
  end
end

export Port

function sendmsg(p::Port,type::Int,msg::String)
  err=ccall(p.dls_sendpkt,Cint,(Cint,Cstring),type,msg)
  if(err!=0)
    error(@sprintf("Could not send msg %d (%s)",type,msg))
  end
end

function loop(p::Port)
  retv=ccall(p.dls_recvpkt,Cstring,())
  if(retv!=C_NULL)
    a=split(unsafe_string(retv),",")
    time=parse(Int,a[1])
    msg=parse(Int,a[2])

    if(msg==MSG_STATE)
      p.sts1=parse(Int,a[3])
      p.sts2=parse(Int,a[4])
      p.sts3=parse(Int,a[5])      
      return nothing
    end

    strings=map(a[3:end]) do s
      convert(String,s)
    end
    return (time,msg,strings)
  end
  nothing
end

function close(p::Port)
  err=ccall(p.dls_close,Cint,())
  if(err!=0)
    error("Could not close")
  end
end

end # Modul end
